# !/bin/bash


monitor=${1:-0}
geometry=( $(herbstclient monitor_rect "$monitor") )
if [ -z "$geometry" ] ;then
    echo "Invalid monitor $monitor"
    exit 1
fi
# geometry has the format: WxH+X+Y
x=${geometry[0]}
y=${geometry[1]}
width=${geometry[2]}
height=16
#font="-*-fixed-medium-*-*-*-11-*-*-*-*-*-*-*"
font="-Gohu-GohuFOnt-Medium-R-Normal--11-80-100-100-C-60-ISO10646-1"
#font="-xos4-terminus-medium-r-normal--12-120-72-72-c-60-iso10646-1"
bgcolor='#150D01'
coloricon='#ff3030'
function uniq_linebuffered() {
    awk '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

herbstclient pad $monitor $height
{
    # events:
    #mpc idleloop player &
    while true ; do
        date +'date  %a %d-%b-^fg(#efefef)%y'
        sleep 1 || break
    done > >(uniq_linebuffered)  &
    childpid=$!
    herbstclient --idle
    kill $childpid
} 2> /dev/null | {
    TAGS=( $(herbstclient tag_status $monitor) )
    date=""
    while true ; do
        bordercolor="#26221C"
        hintcolor="#150D01"
        separator="^fg(#141414)^ro(1x$height)^fg()"
        # draw tags
        for i in "${TAGS[@]}" ; do
            case ${i:0:1} in
                '#')
                    echo -n "^bg(#bc002f)^fg(#141414)"
                    ;;
                '+')
                    echo -n "^bg(#D6A736)^fg(#141414)"
                    ;;
                ':')
                    echo -n "^bg(#ffad60)^fg(#141414)"
                    ;;
                '!')
                    echo -n "^bg(#FF0675)^fg(#141414)"
                    ;;
                *)
                    echo -n "^bg()^fg()"
                    ;;
            esac
            echo -n "^ca(1,herbstclient focus_monitor $monitor && "'herbstclient use "'${i:1}'") '"${i:1} ^ca()"
            echo -n "$separator"
        done
        echo -n "^bg()^p(_CENTER)"
        # small adjustments
       	
        time=$(date +"%T")
        vol=$(amixer  get Master | awk '/Mono.+/ {print $6=="[off]"?$6:$4}' | tr -d '[]')
		battery=$(expr $(expr $(cat /sys/class/power_supply/BAT*/charge_now) \* 100) / $(cat /sys/class/power_supply/BAT*/charge_full))
        if [ "$battery" = "/" ] ;then
            right="$separator^bg($hintcolor) $date $separator"
        else
            right="$separator^bg($hintcolor) ^ca(1,amixer set Master 5%+)^ca(3,amixer set Master 5%-)^fg($coloricon)^i(/home/void/Dropbox/icons/volume.xbm)^fg() $vol^ca()^ca() | ^fg($coloricon)^i(/home/void/Dropbox/icons/battery-full.xbm)^fg() $battery%| $time | $date | $separator^ca(1,/usr/bin/dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop)^fg($coloricon)^i(/home/void/Dropbox/icons/poweroff.xbm)^fg()^ca() |  $separator"
        fi
        right_text_only=$(echo -n "$right"|sed 's.\^[^(]*([^)]*)..g')
        # get width of right aligned text.. and add some space..
        width=$(textwidth "$font" "$right_text_only  ")
        echo -n "^p(_RIGHT)^p(-300-$width)$right"
        echo
        # wait for next event
        read line || break
        cmd=( $line )
        # find out event origin
        case "${cmd[0]}" in
            tag*)
                #echo "reseting tags" >&2
                TAGS=( $(herbstclient tag_status $monitor) )
                ;;
            date)
                #echo "reseting date" >&2
                date="${cmd[@]:1}"
                ;;
            quit_panel)
                exit
                ;;
            reload)
                exit
                ;;
            #player)
            #    ;;
        esac
        done
} 2> /dev/null | dzen2 -w $width -x $x -y $y -fn "$font" -h $height \
    -ta l -bg "$bgcolor" -fg '#efefef'
